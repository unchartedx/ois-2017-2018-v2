window.addEventListener('load', function() {
	//stran nalozena

	//Posodobi opomnike
	var posodobiOpomnike = function() {
		var opomniki = document.querySelectorAll(".opomnik");

		for (var i = 0; i < opomniki.length; i++) {
			var opomnik = opomniki[i];
			var casovnik = opomnik.querySelector("span");
			var cas = parseInt(casovnik.innerHTML);

			//TODO:
			// - če je čas enak 0, izpiši opozorilo "Opomnik!\n\nZadolžitev NAZIV_OPOMNIK je potekla!"
			// - sicer zmanjšaj čas za 1 in nastavi novo vrednost v časovniku
		}
	}
	setInterval(posodobiOpomnike, 1000);
	
	var prijavaUporabnika = function(){
		
		var uporabnik=document.querySelector("#uporabnisko_ime").value;
		
		document.querySelector("#uporabnik").innerHTML = uporabnik;
		document.querySelector(".pokrivalo").style.visibility = "hidden";
		
	}
	
	document.querySelector("#prijavniGumb").addEventListener('click', prijavaUporabnika);
	
	var dodajOpomnik =function(){
		
		var naziv_opomnika = document.querySelector("#naziv_opomnika").value; //pridobivanje vrednosti polja naziv_opomnika
		document.querySelector("#naziv_opomnika").value = ""; //ponastavitev polja na blank
		
		var cas_opomnika = document.querySelector("#cas_opomnika").value; //pridobivanje vrednosti za čas opomnika
		document.querySelector("#cas_opomnika").value = "";
		
		var opomniki = document.querySelector("#opomniki");
		
		document.getElementById('opomniki').innerHTML +=
			'<div class = "opomnik"> <div class = "naziv_opomnika">  '+ naziv_opomnika +'  </div>	<div class = "cas_opomnika"> Opomnik čez <span> '+ cas_opomnika +' </span> sekund. </div> <div>';
		
	}
	
	document.querySelector("#dodajGumb").addEventListener('click', dodajOpomnik); //se odzove na pritisk gumba
	
});
